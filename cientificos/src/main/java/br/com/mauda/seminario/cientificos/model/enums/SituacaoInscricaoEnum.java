package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "Tecnologias de Gestao da Informacao"),
    COMPRADO(2L, "Matematica"),
    CHECKIN(3L, "Linguas");

    private Long id;
    private String nome;

    private SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
