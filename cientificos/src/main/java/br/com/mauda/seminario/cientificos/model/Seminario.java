package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private LocalDate data;
    private int qtdIncricoes;
    private List<AreaCientifica> areaCientifica = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, int qtdInscricoes) {
        super();
        this.adicionarAreaCientifica(areaCientifica);
        this.adicionarProfessor(professor);
        professor.adicionarSeminario(this);
        this.qtdIncricoes = qtdInscricoes;
        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        this.areaCientifica.add(areaCientifica);
    }

    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
        professor.adicionarSeminario(this);
    }

    public boolean possuiAreaCientifica(AreaCientifica areaCientifica) {
        return this.areaCientifica.contains(areaCientifica);
    }

    public boolean possuiIncricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public LocalDate getData() {
        return this.data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getQtdInscricoes() {
        return this.qtdIncricoes;
    }

    public void setQtdIncricoes(int qtdIncricoes) {
        this.qtdIncricoes = qtdIncricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areaCientifica;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Seminario other = (Seminario) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
