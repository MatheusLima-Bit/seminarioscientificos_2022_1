package br.com.mauda.seminario.cientificos.model;

import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao = LocalDateTime.now();
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
    }

    public void comprar(Estudante estudante, boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.adicionarInscricao(this);
        this.direitoMaterial = direitoMaterial;
        this.dataCompra = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void cancelarCompra() {
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.estudante.removeInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.dataCompra = null;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
        this.dataCheckIn = LocalDateTime.now();
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
